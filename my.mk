## db              : db shell
.PHONY: db
db:
	docker-compose exec db bash

## web              : web shell
.PHONY: web
web:
	docker-compose exec web bash

#up:
#	@echo "Starting up containers..."
#	docker-compose pull
#	docker-compose up -d --remove-orphans

start:
	git pull
	git merge origin/master
	docker-compose up -d
	@echo http://localhost/index.php

stop:
	docker-compose stop
	git add -A
	git status
	git commit -m "Update"
	git push -u origin


# https://stackoverflow.com/a/6273809/1826109
%:
	@:
