<?php

session_start();
$query_string = $_SERVER['QUERY_STRING'];

echo "<a href='?login'>Login</a> </br>";
echo "<a href='?logout'>Logout</a> </br>";
echo "<a href='?view-content'>View content</a> </br>";
echo "<a href='?all-users'>View content</a> </br>";

$user = new User();

switch ($query_string){
  case 'all-users':
    $model = Model::getModel();
     $users = $model->getAllUsersDataInArray();
     Helper::helper_print_array($users);
  case 'login':
    $user->login();
    break;
  case 'logout':
    $user->logout();
    break;
  case 'view-content':
    $content = Model::getContent();
    View::showContent($content);
    break;
  case '':
  default:
    echo 'HOM-E';
}

if ($user->isLoggedIn()) {
  Helper::printString("Hi <strong> $user->username </strong> user!");
}
else {
  Helper::printString('Hi anonymous...');
}
/** ----------------------------------------------------------------- */

class View {

  public static function showContent(array $contentArray) {
    echo '<table style="border:1px solid black">';
    echo '<tr>';
    echo '<td style="border:1px solid black"> Title';
    echo '<td style="border:1px solid black"> Actions';
    echo '<tr>';
    foreach ($contentArray as $content) {
      echo "<td style='border:1px solid black'> {$content['title']}";
      echo "<td style='border:1px solid black'>" . implode('| ', Model::getContentActions());
      echo '<tr>';
    }
    echo '</table>';
  }
}

class Helper {

  public static function printString(string $string): void {
    echo "</br>$string</br>";
  }

  public static function helper_print_array(array $array): void {
    echo "</br>-----------------------------";
    foreach ($array as $key => $item) {
      echo "</br> $key => $item";
      if (is_array($item)) {
        self::helper_print_array($item);
      }
    }
  }
}

class Model {

  public static function getModel() {
    return new self();
  }

  public function getAllUsersDataInArray(): array {
    $db = self::getDb();

    $sql = "SELECT * FROM users";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public static function getDb(): PDO {
    try {
      $conn = new PDO('mysql:host=192.168.0.2:3306;dbname=test_db', 'devuser', 'devpass');
      // set the PDO error mode to exception
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
      echo "Connection failed: " . $e->getMessage();
    }
    return $conn;
  }

  public static function getContent(): array {
    $result = [];
    if (User::getUser()->hasPermission('view')) {
      $result = self::getContentData();
    }
    else {
      Helper::printString('No permission to view');
    }

    return $result;
  }

  public static function getContentData() {
    return [
      [
        'title' => 'First content title',
        'body' => "Body of the forst content",
        'admin_only_access' => FALSE,
      ],
      [
        'title' => 'Second, only for admin!',
        'body' => 'Secret content',
        'admin_only_access' => TRUE,
      ]
    ];
  }

  public static function getContentActions() {
    $actions = [];
    $possible_actions = ['view', 'edit', 'delete'];
    $user = User::getUser();
    foreach ($possible_actions as $possible_action) {
      if ($user->hasPermission($possible_action)){
        $actions[] = $possible_action;
      }
    }

    return $actions;
  }

  public static function getUserPermissions(string $username) {
    return ['view', 'delete'];
  }

}

class Fixture {
  public static function createStructure () {
    $db = Model::getDb();
    $sql = "DROP TABLE IF EXISTS users";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $sql = " CREATE TABLE users (username varchar(100) NOT NULL PRIMARY KEY)";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $sql = "INSERT INTO users (username) VALUES
                                    ('peter_rabbit'),
                                    ('john'),
                                    ('linda')";
    $stmt = $db->prepare($sql);
    $stmt->execute();
  }
}

class User {
  public string $username;

  public function __construct() {
    if (array_key_exists('logged_in_username', $_SESSION)){
      $this->username = $_SESSION['logged_in_username'];
    }
    else {
      $this->username = 'anonymous';
    }
  }

  public static function getUser(): User {
    return new self();
  }

  public function login() {
    $_SESSION['logged_in_username'] = 'peter_the_rabbit';
  }

  public function logout() {
    unset ($_SESSION['logged_in_username']);
  }

  public function isLoggedIn() {
    return array_key_exists('logged_in_username', $_SESSION);
  }

  public function hasPermission(string $permission) {
    $user_permissions = Model::getUserPermissions($this->username);
    return in_array($permission, $user_permissions);
  }
}
